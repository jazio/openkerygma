# OpenKerygma

## Description
This is an collaborative tool to Bible Translation from original languages: Koine Greek, Biblical Hebrew (including the use of Tiberian vocalization) and Biblical Aramaic.

## Naming
Kerygma (from the ancient Greek word κήρυγμα kérugma) is a Greek word used in the New Testament for "proclamation" (see Luke 4:18-19, Romans 10:14, Gospel of Matthew 3:1). It is related to the Greek verb κηρύσσω (kērússō), literally meaning "to cry or proclaim as a herald" and being used in the sense of "to proclaim, announce, preach". Amongst biblical scholars, the term has come to mean the core of the early church's teaching about Jesus.

Origins
"Kerygmatic" is sometimes used to express the message of Jesus' whole ministry, as[1] "a proclamation addressed not to the theoretical reason, but to the hearer as a self"; as opposed to the didactic use of Scripture that seeks understanding in the light of what is taught.[2] The meaning of the crucifixion is central to this concept.

Open, it means this project is open to contribution.


## Greek New Testament utilized as source.
https://sblgnt.com/

OpenKerygma NT utilizes SBLGNT (Society of Biblical Literature Greek New Testament). The SBLGNT is edited by Michael W. Holmes, who utilized a wide range of printed editions, all the major critical apparatuses, and the latest technical resources and manuscript discoveries as he established the text. The result is a critically edited text that differs from the Nestle-Aland/United Bible Societies text in more than 540 variation units.

Reasoning for using SBLGNT

Freely Available
The SBLGNT is available as a free download. Students, teachers, pastors, and interested laypersons can use the SBLGNT right now for research, writing, and study—at no cost.


Versatile
The SBLGNT is licensed freely under the Creative Commons Attribution 4.0 International Public License.

Available in Print
A reasonably-priced professionally produced print edition of the SBLGNT is also available directly from SBL. It includes the full apparatus of variant readings from the four primary editions on which the SBLGNT is based.

With Smyth-sewn binding, a Kivar cover, and reader-friendly type, the print edition provides a durable, affordable alternative for scholarly research and classroom use.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
We are open to contributions and what your requirements are for accepting them. Please contact farcaso@gmail.com

OpenKerygma Public Key
RSA 2048 Key for OpenKerygma Project

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhOLuyxLmjk4Oa7SZyr+XEHvOD/UwB/mIpjgGQVabq4OBLlxzM6js2K3mEgz39w+HIXiJu2XMjAD5W8b5F7j97bcgC/6ci0sMZUhQsrTzphxTwjh1Rgrc3ZIHiaxIG0PmzU19hjm9eTxgW5M+P3rr8Og+wa+y8nbK2sm8Wywhj57vZj0dqD2kkznoP1k0aHGdFq8pjTgcdjzbXxEVYM1Qv+P9YRFP0sM2XtyEDBdAgcYvM3mn+HQfizPvZjGE2YJ7X3ZMGY/41Wzm5yHCLM9WlxFeHcTVqwNxMgg0jDZxHll6apcFROrWIegEKeioXVvDoxvgABvxT4b7udec5HoXL RSA

Fingerprints
MD5: 40:c8:14:d0:4f:64:27:09:6d:4b:36:63:1e:b7:c0:f3
SHA256: PkW0B/uSw825UMohh6v0bJXrYfnmR4Het3nmwTdaHbs

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jazio/openkerygma.git
git branch -M main
git push -uf origin main
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
All rights reserved OpenKerygma 2023.

## Project status
Alpha 0.1
