# 1 Petri 01

1 Πέτρος ἀπόστολος Ἰησοῦ Χριστοῦ ἐκλεκτοῖς παρεπιδήμοις διασπορᾶς Πόντου, Γαλατίας, Καππαδοκίας, Ἀσίας, καὶ Βιθυνίας,

Pétru, apostol al lui Iesus Hristos, celor aleși, străini ai diasporei Pontului, Galatíei, Kapadokíei, Asíei și Bitiníei.

2 κατὰ πρόγνωσιν θεοῦ πατρός, ἐν ἁγιασμῷ πνεύματος, εἰς ὑπακοὴν καὶ ῥαντισμὸν αἵματος Ἰησοῦ Χριστοῦ · χάρις ὑμῖν καὶ εἰρήνη πληθυνθείη.

după cunoașterea mai dinainte a lui Dumnezeu Tatăl spre ascultare și stropire cu sângele lui Iesus Hristos.

Har vouă și pace îmbelșugată!

3 Εὐλογητὸς ὁ θεὸς καὶ πατὴρ τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ, ὁ κατὰ τὸ πολὺ αὐτοῦ ἔλεος ἀναγεννήσας ἡμᾶς εἰς ἐλπίδα ζῶσαν δι’ ἀναστάσεως Ἰησοῦ Χριστοῦ ἐκ νεκρῶν,

Preamărit *să fie* Dumnezeu și Tatăl Domnului nostru Iesus Hristos, cel care după multa lui îndurare ne-a născut din nou la o speranță vie prin învierea lui Iesus Hristos din morți, 

4 εἰς κληρονομίαν ἄφθαρτον καὶ ἀμίαντον καὶ ἀμάραντον, τετηρημένην ἐν οὐρανοῖς εἰς ὑμᾶς

la o moștenire nepieritoare, nepângărită și neofilită păstrată în ceruri pentru voi.

5 τοὺς ἐν δυνάμει θεοῦ φρουρουμένους διὰ πίστεως εἰς σωτηρίαν ἑτοίμην ἀποκαλυφθῆναι ἐν καιρῷ ἐσχάτῳ.

Voi sunteți păstrați în puterea lui Dumnezeu prin credință—spre mântuirea pregătită să fie descoperită în vremea din urmă—

6 ἐν ᾧ ἀγαλλιᾶσθε, ὀλίγον ἄρτι εἰ δέον λυπηθέντες ἐν ποικίλοις πειρασμοῖς,

în care voi vă înveseliți mult, deși dacă trebuie sunteți întristați acum *trecând* prin felurite încercări,

7 ἵνα τὸ δοκίμιον ὑμῶν τῆς πίστεως πολυτιμότερον χρυσίου τοῦ ἀπολλυμένου διὰ πυρὸς δὲ δοκιμαζομένου εὑρεθῇ εἰς ἔπαινον καὶ δόξαν καὶ τιμὴν ⸃ ἐν ἀποκαλύψει Ἰησοῦ Χριστοῦ.

pentru ca testul°credinței, mai valoroase ca aurul topit în foc pentru atestare să fie găsit spre recunoaștere, laudă și prețuire la arătarea lui Iesus Hristos.

°
[δοκίμιον] intenționat ales un termen academic în contrast cu sinonimul [πειρασμοῖς] din versetul anterior și pentru potrivire cu δοκιμαζομένου din a doua jumătate a versetului 7.


8 ὃν οὐκ ἰδόντες ἀγαπᾶτε, εἰς ὃν ἄρτι μὴ ὁρῶντες πιστεύοντες δὲ ἀγαλλιᾶσθε χαρᾷ ἀνεκλαλήτῳ καὶ δεδοξασμένῃ,

Pe care fără să-l fi văzut îl iubiți, în care chiar acum deși nu-l vedeți credeți și vă înveseliți cu o bucurie negrăită și plină de glorie

9 κομιζόμενοι τὸ τέλος τῆς πίστεως ὑμῶν σωτηρίαν ψυχῶν.

primind finalul credinței voastre: mântuirea sufletelor.

10 Περὶ ἧς σωτηρίας ἐξεζήτησαν καὶ ἐξηραύνησαν προφῆται οἱ περὶ τῆς εἰς ὑμᾶς χάριτος προφητεύσαντες,

Despre această mântuire, profeții care profețeau despre harul dat vouă, cercetau și investigau,

11 ἐραυνῶντες εἰς τίνα ἢ ποῖον καιρὸν ἐδήλου τὸ ἐν αὐτοῖς πνεῦμα Χριστοῦ προμαρτυρόμενον τὰ εἰς Χριστὸν παθήματα καὶ τὰς μετὰ ταῦτα δόξας ·

căutând /să afle/ cam în ce vreme arăta în ei Duhul lui Hristos când mărturisea despre patimile lui Hristos și despre gloria de după acestea.


12 οἷς ἀπεκαλύφθη ὅτι οὐχ ἑαυτοῖς ὑμῖν δὲ διηκόνουν αὐτά, ἃ νῦν ἀνηγγέλη ὑμῖν διὰ τῶν εὐαγγελισαμένων ὑμᾶς πνεύματι ἁγίῳ ἀποσταλέντι ἀπ’ οὐρανοῦ, εἰς ἃ ἐπιθυμοῦσιν ἄγγελοι παρακύψαι.

Lor li se descoperea că nu pentru ei înșiși slujeau *și profețeau* aceste *lucruri—* care acum vă sunt vestite vouă de către cei ce vă vestesc prin Duhul Sfânt trimis din cer— *lucruri* în care chiar și îngerii doresc să privească.

13 Διὸ ἀναζωσάμενοι τὰς ὀσφύας τῆς διανοίας ὑμῶν, νήφοντες τελείως, ἐλπίσατε ἐπὶ τὴν φερομένην ὑμῖν χάριν ἐν ἀποκαλύψει Ἰησοῦ Χριστοῦ.

Prin urmare încingeți-vă coapsele minții
fiți veghetori până la final, aveți speranță în ceea ce v-a adus har prin arătarea lui Iesus Hristos.


14 ὡς τέκνα ὑπακοῆς, μὴ συσχηματιζόμενοι ταῖς πρότερον ἐν τῇ ἀγνοίᾳ ὑμῶν ἐπιθυμίαις,

Asemeni unor copii ascultători nu vă conformați stării de dinainte în ignoranța poftelor voastre


15 ἀλλὰ κατὰ τὸν καλέσαντα ὑμᾶς ἅγιον καὶ αὐτοὶ ἅγιοι ἐν πάσῃ ἀναστροφῇ γενήθητε,

ci după cum cel ce va chemat e sfânt deveniți și voi sfinți în orice purtare.

16 διότι γέγραπται ὅτι Ἅγιοι ἔσεσθε, ὅτι ἐγὼ ἅγιος.

pentru că este scris:

Sfinți să fiți pentru că eu sunt sfânt!


17 Καὶ εἰ πατέρα ἐπικαλεῖσθε τὸν ἀπροσωπολήμπτως κρίνοντα κατὰ τὸ ἑκάστου ἔργον, ἐν φόβῳ τὸν τῆς παροικίας ὑμῶν χρόνον ἀναστράφητε ·

Și dacă Tatăl—care v-a chemat fără să țină cont de fața omului—judecă după faptele fiecăruia, purtați-vă cu frică în pribegia voastră


18 εἰδότες ὅτι οὐ φθαρτοῖς, ἀργυρίῳ ἢ χρυσίῳ, ἐλυτρώθητε ἐκ τῆς ματαίας ὑμῶν ἀναστροφῆς πατροπαραδότου,

înțelegând că nu cu lucruri pieritoare din argint sau aur ați fost răscumpărați din purtarea voastră fără valoare moștenită de la părinți

19 ἀλλὰ τιμίῳ αἵματι ὡς ἀμνοῦ ἀμώμου καὶ ἀσπίλου Χριστοῦ,

ci cu sângele prețios al lui Hristos, ca al unui miel fără cusur și fără vină.

20 προεγνωσμένου μὲν πρὸ καταβολῆς κόσμου, φανερωθέντος δὲ ἐπ’ ἐσχάτου τῶν χρόνων δι’ ὑμᾶς

El fusese cunoscut mai dinainte încă de la crearea lumii, dar a fost făcut cunoscut în vremurile din urmă prin voi[].

[] prin voi și nu pentru voi deoarece voi stă in acuzativ nu genitiv

21 τοὺς δι’ αὐτοῦ πιστοὺς εἰς θεὸν τὸν ἐγείραντα αὐτὸν ἐκ νεκρῶν καὶ δόξαν αὐτῷ δόντα, ὥστε τὴν πίστιν ὑμῶν καὶ ἐλπίδα εἶναι εἰς θεόν.

voi care prin el credeți în Dumnezeu care l-a înviat din morți. El i-a dat glorie așa încât credința și speranța voastră să fie în Dumnezeu.


22 Τὰς ψυχὰς ὑμῶν ἡγνικότες ἐν τῇ ὑπακοῇ τῆς ἀληθείας εἰς φιλαδελφίαν ἀνυπόκριτον ἐκ καρδίας ἀλλήλους ἀγαπήσατε ἐκτενῶς,

Sufletele voastre purificate în ascultarea de adevăr spre iubirea frățească neprefăcută, iubiți-vă unii pe alții din ° toată inima.

° cuvânt unic în NT derivat de la 'o mână întinsă'


23 ἀναγεγεννημένοι οὐκ ἐκ σπορᾶς φθαρτῆς ἀλλὰ ἀφθάρτου, διὰ λόγου ζῶντος θεοῦ καὶ μένοντος ·

Voi ați fost născuți din nou nu dintr-o sămânță care putrezește ci din una care nu putrezește prin cuvântul viu —care rămâne –al lui Dumnezeu


24 διότι πᾶσα σὰρξ ὡς χόρτος, καὶ πᾶσα δόξα αὐτῆς ὡς ἄνθος χόρτου · ἐξηράνθη ὁ χόρτος, καὶ τὸ ἄνθος ἐξέπεσεν ·

căci orice trup se trece ca iarba și toată gloria lui ca floarea ierbii, iarba se ofilește, iar floarea cade.

25 τὸ δὲ ῥῆμα κυρίου μένει εἰς τὸν αἰῶνα. τοῦτο δέ ἐστιν τὸ ῥῆμα τὸ εὐαγγελισθὲν εἰς ὑμᾶς.

dar cuvântul Domnului rămâne în veac. Acesta cuvânt al evangheliei e pentru voi.
